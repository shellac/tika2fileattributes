## Building

    $ mvn clean compile

This will create `target/tika2fileattributes-1.0-SNAPSHOT-jar-with-dependencies.jar`,
which is a bit of a mouthful, so you might want to rename it.

## Using

   $ java -jar tika2fileattributes.jar DIR+

This will use tika to extract the metadata of files in the provided directories 
and add them to the extended file attributes.
