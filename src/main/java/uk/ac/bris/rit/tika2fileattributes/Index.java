package uk.ac.bris.rit.tika2fileattributes;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.UserDefinedFileAttributeView;
import org.apache.tika.Tika;
import org.apache.tika.metadata.Metadata;

/**
 *
 * @author pldms
 */
public class Index {

    public static void main(String[] args) throws IOException {
        final Index indexer = new Index(Charset.forName("UTF-8"));

        for (String filename : args) {
            Path file = Paths.get(filename);
            if (!Files.exists(file)) {
                System.err.printf("No such file: <%s>\n", file);
            } else if (Files.isDirectory(file)) {
                System.err.printf("Indexing directory <%s>\n", file);
                Files.walkFileTree(file, new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult visitFile(Path subfile, BasicFileAttributes attrs) throws IOException {
                        indexer.index(subfile);
                        return FileVisitResult.CONTINUE;
                    }
                });
            } else {
                System.err.printf("Indexing file <%s>\n", file);
                indexer.index(file);
            }
        }
    }

    private final Charset charset;
    private final Tika tika;

    private Index(Charset charset) {
        this.charset = charset;
        this.tika = new Tika();
    }

    private void index(Path file) throws IOException {
        Metadata metadata = new Metadata();

        // We don't bother with the parsed text, just want the metadata
        try (InputStream in = Files.newInputStream(file);
                Reader reader = tika.parse(in, metadata)) {
                        
            storeMetadataInAttributes(file, metadata);
        }
    }

    private void storeMetadataInAttributes(Path file, Metadata metadata) throws IOException {
        UserDefinedFileAttributeView attributes = 
                Files.getFileAttributeView(file, UserDefinedFileAttributeView.class);
        
        //System.err.printf("<%s> has %d fields\n", file, metadata.size());
        
        for (String name: metadata.names()) {
            if (metadata.isMultiValued(name)) {
                System.err.printf("Warning: metadata field '%s' in <%s> is multivalued and can't be stored\n", name, file);
            } else {
                attributes.write(name, toBytes(metadata.get(name)));
            }
        }
    }

    private ByteBuffer toBytes(String value) {
        return charset.encode(value);
    }
}
